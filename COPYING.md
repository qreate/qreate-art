<h1 align="center">
	Copying terms
</h1>

This work is currently hosted in a Codeberg repository at the following URL :  
https://codeberg.org/qreate/qreate-art

The files contained within this repository inherit the terms and conditions of the [Cooperative Non-violent Public License](LICENSE.md)(CNPL), unless otherwise specified below. <sup>[NPL Official Page](https://thufie.lain.haus/NPL.html)</sup>

---

- ### `icon/`
This directory contains art that is heavily inspired by : the [QuiltMC](https://quiltmc.org/en/) logo, the [Create](https://github.com/Creators-of-Create/Create) logo and the Create "cogwheel" 3D model. The content of said folder does inherit from the CNPL terms.