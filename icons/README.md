<img align="right" width="96px" src="icon-cog.png">

## icon-cog
this icon is recommended if the displaying resolution will be over 96x96px.

an animated version can also be rendered from the icon-cog.blend, i would recommend enableing "Motion Blur" for a better result on playback.


<img align="right" width="96px" src="icon-small.png">

## icon-small
this icon is recommended if the displaying resolution will be under 96x96px.

##### see [COPYING.md](../COPYING.md) for license details